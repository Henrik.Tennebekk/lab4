package no.uib.inf101.gridview;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import javax.swing.JPanel;


public class GridView extends JPanel{
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  IColorGrid colorGrid;
 
  // Konstruktøren
  public GridView(IColorGrid colorGrid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;
  }

  // Metode som skal tegne rutenettet
  public void drawGrid(Graphics2D graphics2d) {
    Rectangle2D rectangle = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2 * OUTERMARGIN, getHeight() - 2 * OUTERMARGIN);
    graphics2d.setColor(MARGINCOLOR);
    graphics2d.fill(rectangle);

    // Lager nytt cellPositionToPixelConverter objekt
    CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(rectangle, colorGrid, OUTERMARGIN);
    
    // Kaller på drawCells metoden for å tegne kvadratene
    drawCells(graphics2d, colorGrid, cellPositionToPixelConverter);
  }


  // Metode som skal tegne rutene
  private static void drawCells(Graphics2D graphics2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPos) {
    List<CellColor> cellColorList = cellColorCollection.getCells();
    for (CellColor cell : cellColorList) {
      // Bruker CellPositionToPixelConverter for å finne posisjonen og lage kvadrat
      Rectangle2D rectangle2d = cellPos.getBoundsForCell(cell.cellPosition());
      Color color = cell.color();
      if (cell.color() == null) {
        color = Color.DARK_GRAY;
      }

      // Setter fargen til kvadratet
      graphics2d.setColor(color);
      graphics2d.fill(rectangle2d);
    }
  }



  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }
}
