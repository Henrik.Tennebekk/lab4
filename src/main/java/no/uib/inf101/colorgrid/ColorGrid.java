package no.uib.inf101.colorgrid;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  // Feltvariabler
  int rows;
  int columns;
  Color color;
  ArrayList<ArrayList<Color>> colorList;
    

  // Konstruktøren
  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.columns = cols;

    colorList = new ArrayList<ArrayList<Color>>();
    for (int i = 0; i < rows; i++) {
      ArrayList<Color> row = new ArrayList<Color>();
      for (int j = 0; j < cols; j++) {
        row.add(null);
      }
      colorList.add(row);
    }
  }

  // Metode for å finne antall rader
  @Override
  public int rows() {return colorList.size();}

  // Metode for å finne antall kolonner
  @Override
  public int cols() {return (colorList.get(0)).size();}

  // Metode for å finne verdien til et punkt
  @Override
  public Color get(CellPosition pos) {return (colorList.get(pos.row())).get(pos.col());}

  // Metode for å sette verdien til et punkt
  @Override
  public void set(CellPosition pos, Color color) {colorList.get(pos.row()).set(pos.col(), color);}

  // Metode for å hente celler sine fargeverdier
  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColors = new ArrayList<>();
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < columns; col++) {
        CellPosition pos = new CellPosition(row, col);
        cellColors.add(new CellColor(pos, get(pos)));
      }
    }
    return cellColors;
  }
}
